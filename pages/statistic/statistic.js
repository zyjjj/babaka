import Line from '../../XY/XY.js';
const conf = {
  data: {
  },
  onLoad: function () {
    var xValue = [];
    for (var i = 1; i <= 31; i++) {
      xValue.push(i);
    }
    var yValue = [100,58.2,44.2,35.8,33.7,27.8,25.4,25.3,25.3,25.2,25.1,25,24.9,24.8,24.7,24.6,24.5,24.4,24.3,24.2,24.1,24,20,15,10,5,2,1,0.2];
    Line.lineCreate('lineChart', xValue, yValue, 200);
  },
    goMy: function () {
    wx.switchTab({
      url: '../my/my',
    })
  }
};
Page(conf);
