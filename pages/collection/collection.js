// pages/collection/collection.js
var app = getApp()  
Page({
  data: {
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  showWord: function (event) {
    wx.navigateTo({
      url: '../det_word/det_word?word='+event.currentTarget.id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    //更新数据  
    wx.request({
      url: 'https://407848871.jmubabaka.xyz/babaka-common/servlet',
      headers: { 'Content-Type': 'application/json' },
      data: {
        num: '6',
        userid: getApp().globalData.openid
      },
      success: function (res) {
        console.log(res);
        //将获取到的json数据，存在名字叫wList中
        that.setData({
          wList: res.data,
        })
      },
      fail: function () {
        console.log("....fail....")
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})