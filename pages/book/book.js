//pages/book/book.js
Page({

  data: {
    book:"",
    id:0,
    arrayfy:["英音","美音"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  bindViewTap: function (event) {
    var that=this
    wx.navigateTo({
      url: '../study/study?book=' + event.currentTarget.id+'&id='+that.data.id,
      success: function (e) {

        var page = getCurrentPages().pop();
        if (page == undefined || page == null) return;
        page.onLoad();
      }
    })
  },
  changeTap: function (event) {
    var that = this;
    getApp().globalData.fyid = event.detail.value
    that.setData({
      id: event.detail.value
    })
  }
})