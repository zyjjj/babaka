// pages/det_word/det_word.js
var Parser = require('../../lib/xmldom/dom-parser')
var op;
Page({

  data: {
    word: "",
    book: "",
    isShow: false,
    fyid:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (option) {

    var that = this;
    op = option;
    that.wexplain(option.word);
    that.setData({
      fyid: getApp().globalData.id,
      book: option.word,
      //isShow: true
    })
  },

  bindViewTap: function () {

    wx.navigateTo({
      url: '../study/study?book=' + op.book,
      success: function (e) {
        var page = getCurrentPages().pop();
        if (page == undefined || page == null) return;
        page.onLoad();
      }
    })
  },
  //删除单词
  deleteWordTap: function (event) {
    var that = this;
    wx.request({
      url: 'https://407848871.jmubabaka.xyz/babaka-common/servlet',
      header: {
        'content-type': 'application/json' // 默认值
      },
      data: {
        num: '7',
        userid: getApp().globalData.openid,
        word: event.currentTarget.id
      },
      success: function (res) {
        console.log("删除 " + res.data);
        wx.showToast({
          title: '删除成功',
          icon: 'success',
          duration: 2000
        })
        wx.navigateBack();
      },
      fail: function (res) {
        console.log(".....fail.....");
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (option) {
    /*
    console.log("fdsafas");
    //console.log(option.target.dataset.book);
    var book="";
    console.log(op)
    if(op==null){
      book = option.currentTarget.id;
    }
    else{ book=op.book };
    console.log("book="+book);
    var that=this;
    that.bindtest(book);
    */
    //that.onLoad(op);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  wexplain: function (option) {
    var that = this;
    wx.request({
      url: 'https://407848871.jmubabaka.xyz/babaka-common/servlet',
      data: {
        num: '3',
        book: option,
        word: ""
      },
      method: 'GET',
      header: {
        'Content-Type': 'application/xml'
      },
      success: function (res) {
        var XMLParser = new Parser.DOMParser();
        var doc = XMLParser.parseFromString(res.data);
        var ps = doc.getElementsByTagName("ps")[getApp().globalData.fyid];
        var pron = doc.getElementsByTagName("pron")[getApp().globalData.fyid];
        var pos = doc.getElementsByTagName("pos")[0];
        var acceptation = doc.getElementsByTagName("acceptation")[0];
        var orig = doc.getElementsByTagName("orig")[1];
        var trans = doc.getElementsByTagName("trans")[1];
        that.setData({
          word: {
            name: option,
            ps: ps.firstChild.nodeValue,
            pos: pos.firstChild.nodeValue,
            pron: pron.firstChild.nodeValue,
            acceptation: acceptation.firstChild.nodeValue,
            orig: orig.firstChild.nodeValue,
            trans: trans.firstChild.nodeValue
          }
        });
      }
    }),
      that.setData({
        fyid:getApp().globalData.fyid,
        isShow: true
      })
  }

})