// pages/study/study.js
var Parser=require('../../lib/xmldom/dom-parser')
var op;
Page({

  data: {
    id:0,
    word:"",
    book:"",
    isShow: false,
  },
  audioPlay: function () {
    this.audio.play()
  },
  audioPause: function () {
    this.audio.pause()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (option) {
    var that = this;
    op=option;
    console.log("11"+option.id);
    that.setData({
      book:option.book,
      id:option.id
      //isShow: true
    })
  },
  
  bindViewTap: function () {

    wx.navigateTo({
      url: '../study/study?book=' + op.book,
      success: function (e) {
        var page = getCurrentPages().pop();
        if (page == undefined || page == null) return;
        page.onLoad();
      } 
    })
  },
 //收藏单词
  collectWordTap: function (event) {
    var that = this;
    wx.request({
      url: 'https://407848871.jmubabaka.xyz/babaka-common/servlet',
      header: {
        'content-type': 'application/json' // 默认值
      },
      data: {
        num: '5',
        userid: getApp().globalData.openid,
        word: event.currentTarget.id
      },
      success: function (res) {
        console.log("收藏 "+res.data);
        wx.showToast({
          title: '收藏成功',
          icon: 'success',
          duration: 2000
        })
      },
      fail: function (res) {
        console.log(".....fail.....");
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 使用 wx.createAudioContext 获取 audio 上下文 context
    this.audio = wx.createAudioContext('myAudio')
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (option) {

    //console.log(option.target.dataset.book);
    var book="";
    console.log(op)
    if(op==null){
      book = option.currentTarget.id;
    }
    else{ book=op.book };
    console.log("book="+book);
    var that=this;
    that.bindtest(book);
    //that.onLoad(op);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  bindtest: function(option) {

    var that=this;
    wx.request({
      url: 'https://407848871.jmubabaka.xyz/babaka-common/servlet',
      data: {
        num: '2',
        book: option,
        name:""
      },
      method: 'GET',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:function (res) {
      that.wexplain(res.data);
        that.setData({
          name: res.data
        })
      },
      fail: function (res) {
        console.log(".....fail.....");
      }
    })  
  },
 
  wexplain: function(option) {
    var that=this;
    wx.request({
      url: 'https://407848871.jmubabaka.xyz/babaka-common/servlet',
      data: {
        num:'3',
        book:option,
        word: ""
      },
      method: 'GET',
      header: {
        'Content-Type': 'application/xml'
      },
      success: function(res) {
        console.log(that.id);
        var XMLParser = new Parser.DOMParser();
        var doc = XMLParser.parseFromString(res.data);
        var ps = doc.getElementsByTagName("ps")[that.data.id];
        var pron = doc.getElementsByTagName("pron")[that.data.id];
        var pos = doc.getElementsByTagName("pos")[0];
        var acceptation = doc.getElementsByTagName("acceptation")[0];
        var orig = doc.getElementsByTagName("orig")[1];
        var trans = doc.getElementsByTagName("trans")[1];
        that.setData({
          word:{
            name: option,
            ps: ps.firstChild.nodeValue,
            pos: pos.firstChild.nodeValue,
            pron: pron.firstChild.nodeValue,
            acceptation: acceptation.firstChild.nodeValue,
            orig: orig.firstChild.nodeValue,
            trans:trans.firstChild.nodeValue
          }
        });
        console.log("ps:" + ps.firstChild.nodeValue);
      }
    }),
    that.setData({
      isShow: true
    })
  }
})