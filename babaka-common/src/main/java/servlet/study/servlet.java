package servlet.study;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.net.www.URLConnection;
import word.WordManager;
import word.Word;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;  
  
@WebServlet("/servlet")
public class servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public servlet() {
        
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html;charset=utf-8");  
        /* 设置响应头允许ajax跨域访问 */  
        response.setHeader("Access-Control-Allow-Origin", "*");
        /* 星号表示所有的异域请求都可以接受， */  
        response.setHeader("Access-Control-Allow-Methods", "GET,POST");  
        Writer out = response.getWriter();
        //获取微信小程序get的参数值并打印
        String num = request.getParameter("num");
        String book = request.getParameter("book");
        
        if(num!=null&&num.equals("1")){
        	 out.write(getSentenceInform("http://open.iciba.com/dsapi/"));
        }
        else if(num!=null&&book!=null&&num.equals("3")){
       	 	out.write(getSentenceInform("http://dict-co.iciba.com/api/dictionary.php?w="+book+"&key=75F00367008C3B5F0B35ACBFD3276941"));
       }else if(num!=null&&num.equals("4")){

    	   String code = request.getParameter("code");
    	 //调用request请求api转换登录凭证
      	   out.write(getSentenceInform("https://api.weixin.qq.com/sns/jscode2session?appid=wxcdd663bedc71fae3&secret=950a6f1cd2d3b25c4ec79ca56a39709a&grant_type=authorization_code&js_code="+ code));
      }else if(num!=null&&num.equals("5")){

   	   		String userid = request.getParameter("userid");
   	   		String word = request.getParameter("word");
   	   		//传入用户数据，单词
   	   		boolean flag=new WordManager().setcollWord(word, userid);
   	   		if(flag=true){
   	   			out.write("成功");
   	   		}else{
   	   			out.write("失败");
   	   		}
     	   
      }else if(num!=null&&num.equals("6")){
    	  
    	  String userid = request.getParameter("userid");
    	  ArrayList wlist  = new WordManager().getcollWord(userid);
    	  int count = wlist.size();
    	  StringBuilder s = new StringBuilder("{\"count\":"+count+",\"word\":[");
    	  for(int i = 0; i<wlist.size(); i++){ 
    		  s.append("\""+wlist.get(i)+"\"");
    		  if(i!=wlist.size()-1){
    			  s.append(",");
    		  }
    	  }
    	  s.append("]}");
    	  String wordjson = s.toString();
 	   	  //传入用户数据，单词
 	   	  out.write(wordjson);
 	   	  
     }else if(num!=null&&num.equals("7")){

	   		String userid = request.getParameter("userid");
	   		String word = request.getParameter("word");
	   		//删除用户数据，单词
	   		boolean flag=new WordManager().delcollWord(word, userid);
	   		if(flag=true){
	   			out.write("成功");
	   		}else{
	   			out.write("失败");
	   		}
  	   
   }else{
	        List<String> word =new ArrayList();
	        WordManager wordm;
	        //返回值给微信小程序
	        if(book!=null)
	        	wordm=new WordManager(WordManager.class.getClassLoader().getResource("lexicon").getPath()+book+".txt");
	        else
	        	wordm=new WordManager(WordManager.class.getClassLoader().getResource("lexicon").getPath()+"CET4.txt");
	        Word s = wordm.getRandomWord();
	
	        word.add(s.name);
	        word.add(s.phonetic);
	        word.add(s.interp);
	        out.write(word.get(0));
        }
        out.flush();

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}
    public static String getSentenceInform(String cbUrl){  
        
        //词霸API
        StringBuffer strBuf = new StringBuffer();
              
        try{  
            URL url = new URL(cbUrl);
            java.net.URLConnection conn = url.openConnection();  
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));//转码。  
            String line = null;  
            while ((line = reader.readLine()) != null)  
                strBuf.append(line + " ");  
                reader.close();  
        }catch(MalformedURLException e) {  
            e.printStackTrace();   
        }catch(IOException e){  
            e.printStackTrace();   
        }
        return strBuf.toString();  
    } 
}