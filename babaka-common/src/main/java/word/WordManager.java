package word;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import servlet.study.servlet;

public class WordManager {
	private String lexPath;
	private String lexName;
	private int indexBound;	// 词库中记录总条数
	
	public WordManager(String lexPath) throws IOException {
		this.lexPath = lexPath;
		calcIndexBound();
	}
	public WordManager(){
	}
	// 从词库中随机去取出单词
	public Word getRandomWord() throws IOException {
		Word word = null;
		Random random = new Random(System.currentTimeMillis());
		int index = random.nextInt(indexBound);
		FileReader fr = new FileReader(lexPath);
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();// 词库名称
		// 跳过前面的内容
		for (int i = 0; i < 4 * index; i++) {
			line = br.readLine();
		}
		// TODO: 有越界的 BUG
		// 定位到单词条目开始处
		while (line != null && !line.equals("")) {
			line = br.readLine();
		}
		do {
			line = br.readLine();
		}
		while (line != null && line.equals(""));
		
		ArrayList<String> wordItem = new ArrayList<String>();
		// 直到单词条目结束
		while (line !=null && !line.equals("")) {
			wordItem.add(line);
			line = br.readLine();
		}
		switch(wordItem.size()) {
		case 2:
			word = new Word();
			word.name = wordItem.get(0);
			word.phonetic = " ";
			word.interp = wordItem.get(1);
			break;
		case 3:
			word = new Word();
			word.name = wordItem.get(0);
			word.phonetic = wordItem.get(1);
			word.interp = wordItem.get(2);
			break;
		default:
			break;
		}
		br.close();
		fr.close();
		return word;
	}
	//收藏单词
	public boolean setcollWord(String collword,String user) throws IOException {

		int flag = 0;
		String s = collword+"\r\n";  
		String Path = "c:\\collectionWord\\"+user+".txt";
		FileWriter fWriter=new FileWriter(Path, true);
		FileReader fr = new FileReader(Path);
		BufferedReader br = new BufferedReader(fr);
		String t;
		while((t=br.readLine())!=null){
			if(t.equals(collword)){
				flag=1;
				break;
			}
		}
		if(flag==0){
			try {
		    	fWriter.write(s,0,s.length());
			    fWriter.flush();
			    return true;
		    }catch (IOException e) {
		    	e.printStackTrace();
		    	return false;
		    }finally{
		    	fWriter.close();
		    }
		}
		return true;
	}
	//删除收藏单词
	public boolean delcollWord(String collword,String user) throws IOException {
		
		ArrayList wordlist = new ArrayList();
		String Path = "c:\\collectionWord\\"+user+".txt";
		FileWriter fWriter=new FileWriter(Path, true);
		FileReader fr = new FileReader(Path);
		BufferedReader br = new BufferedReader(fr);
		String t;
		while((t=br.readLine())!=null){
			if(!t.equals(collword))
				wordlist.add(t);
		}
		FileWriter fWriter_new=new FileWriter(Path);
		
		try {
			for(int i = 0; i<wordlist.size(); i++){ 
				String s=(String) wordlist.get(i)+"\r\n";
				fWriter_new.write(s,0,s.length());
				fWriter_new.flush();
			}
			return true;
		}catch (IOException e) {
		    	e.printStackTrace();
		    	return false;
		 }finally{
		     fWriter_new.close();
		 }

	}	
	//获取收藏的单词
	public ArrayList getcollWord(String user) throws IOException {

		ArrayList wordlist = new ArrayList();
		String Path = "c:\\collectionWord\\"+user+".txt";
		FileWriter fWriter=new FileWriter(Path, true);
		FileReader fr = new FileReader(Path);
		BufferedReader br = new BufferedReader(fr);
		String t;
		while((t=br.readLine())!=null){
			wordlist.add(t);
		}
		Collections.sort(wordlist);
		return wordlist;
	}
	
	// 返回词库名称
	public String getLexiconName() {
		return lexName;
	}
	
	// 计算词库的单词条数
	private void calcIndexBound() throws IOException {
		FileReader fr = new FileReader(lexPath);
		BufferedReader br = new BufferedReader(fr);
		String line = br.readLine();	// 词库名称
		lexName = line;
		
		int indexBound = 0;
		while (line != null) {
			if (line.equals("")) {
				indexBound++;
			}
			line = br.readLine();
		}
		this.indexBound = indexBound;
		br.close();
		fr.close();
	}
	
}