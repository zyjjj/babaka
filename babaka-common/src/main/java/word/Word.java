package word;

public class Word {
	public String name;
	public String phonetic;
	public String interp;
	
	public Word() {
		
	}
	public String toString(){
		return String.format("Word [name=%s, phonetic=%s, interp=%s]",name,phonetic,interp);
	}
	public Word(String name, String phonetic, String interp) {
		this.name = name;
		this.phonetic = phonetic;
		this.interp = interp;
	}
	
}

